package ru.cleptoman.starling.builder.extension {
	import com.sgn.starlingbuilder.engine.IAssetMediator;
	import com.sgn.starlingbuilder.engine.localization.ILocalization;
	import com.sgn.starlingbuilder.engine.UIBuilder;
	import starling.display.DisplayObject;
	
	/**
	 * ...
	 * @author Aleksey Kutov aka cleptoman
	 */
	public class UIBuilder extends com.sgn.starlingbuilder.engine.UIBuilder {
		
		private var _assets:IAssetMediator;
		
		public function UIBuilder(assetMediator:IAssetMediator, forEditor:Boolean=false, template:Object=null, localization:ILocalization=null) {
			super(assetMediator, forEditor, template, localization);
			_assets = assetMediator;
		}
		
		public function buildByName(name:String):DisplayObject {
			var config:Object = _assets.getExternalData(name);
			if (!config) return null;
			return super.create(config) as DisplayObject;
		}
		
	}

}