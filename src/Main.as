package{
	import flash.display.Sprite;
	import flash.events.Event;
	import starling.events.Event;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author Aleksey Kutov aka cleptoman
	 */
	public class Main extends Sprite {
		
		private var _starling:Starling;
		
		public function Main() {
			if (stage) init();
			else addEventListener(flash.events.Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:flash.events.Event = null):void {
			removeEventListener(flash.events.Event.ADDED_TO_STAGE, init);
			// entry point
			/**
			 * создаем старлинг
			 */
			_starling = new Starling(StarlingRoot,stage);
			_starling.addEventListener(starling.events.Event.ROOT_CREATED, onStarlingRootCreated);
			_starling.start();
		}
		
		private function onStarlingRootCreated(e:starling.events.Event):void {
			_starling.removeEventListener(starling.events.Event.ROOT_CREATED, onStarlingRootCreated);
		}
		
	}
	
}

