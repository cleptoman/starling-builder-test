package {
	import ru.cleptoman.starling.builder.extension.AssetMediator;
	import ru.cleptoman.starling.builder.extension.UIBuilder;
	import starling.display.Sprite;
	
	/**
	 * ...
	 * @author Aleksey Kutov aka cleptoman
	 */
	public class StarlingRoot extends Sprite {
		
		private static const PATH_PREFIX:String = "../workspace/";
		private static const LAYOUT:String = "test_layout";
		private static const LABEL:String = "label";
		private static const RESOURCES:String = "ui";
		private static const FONT:String = "GrilledCheeseBTN_Size18_ColorFFFFFF_StrokeA8364B";
		
		private var _assets:AssetMediator;
		private var _builder:UIBuilder;
		private var _window:Window;
		
		public function StarlingRoot() {
			super();
			/**
			 * create the builder
			 */
			_assets = new AssetMediator();
			_assets.verbose = true;
			
			_builder = new UIBuilder(_assets);
			
			/**
			 * add assets to queue
			 */
			addResourcesToQueue(RESOURCES);
			/**
			 * add fonts to queue
			 */
			addFontToQueue(FONT);
			/**
			 * add component configs to queue
			 */
			addLayoutConfigToQueue(LAYOUT);
			addLayoutConfigToQueue(LABEL);
			
			/**
			 * add classes to compiling
			 */
			Window;
			Label;
			
			/**
			 * start load
			 */
			_assets.loadQueue(onLoadProgress);
		}
		
		
		
		private function onLoadProgress(ratio:Number):void {
			if (ratio < 1) return;
			/**
			 * теперь строим наше окошко
			 */
			_window = _builder.buildByName(LAYOUT) as Window;
			addChild(_window);
		}
		
		private function addFontToQueue(name:String):void {
			_assets.enqueue(PATH_PREFIX + "textures/fonts/"  +name + ".fnt");
			_assets.enqueue(PATH_PREFIX + "textures/fonts/"  +name + ".png");
		}
		
		private function addResourcesToQueue(name:String):void {
			_assets.enqueue(PATH_PREFIX + "textures/"  +name + ".xml");
			_assets.enqueue(PATH_PREFIX + "textures/"  +name + ".png");
		}
		
		private function addLayoutConfigToQueue(name:String):void {
			_assets.enqueue(PATH_PREFIX + "layouts/"  + name + ".json");
		}
		
	}

}