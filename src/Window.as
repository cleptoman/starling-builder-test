package {
	import feathers.display.Scale9Image;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * ...
	 * @author Aleksey Kutov aka cleptoman
	 */
	public class Window extends Sprite {
		
		private static const CLOSE_NAME:String = "generic_exit";
		private static const BUTTON_NAME:String = "generic_button_blue";
		private static const BACK_NAME:String = "generic_check_bg";
		
		private var _button:Button;
		private var _close:Button;
		private var _back:Scale9Image;
		
		public function Window() {
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
		}
		
		private function onAddToStage(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			_button = getChildByName(BUTTON_NAME) as Button;
			_close = getChildByName(CLOSE_NAME) as Button;
			_back = getChildByName(BACK_NAME) as Scale9Image;
			_button.addEventListener(TouchEvent.TOUCH, onTouch);
			_close.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(e:TouchEvent):void {
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.ENDED);
			var touch:Touch;
			for each(touch in touches) {
				trace("click ", touch.target.name);
			}
			
		}
		
	}

}